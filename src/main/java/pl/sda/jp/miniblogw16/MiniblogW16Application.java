package pl.sda.jp.miniblogw16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniblogW16Application {

    public static void main(String[] args) {
        SpringApplication.run(MiniblogW16Application.class, args);
    }

}
